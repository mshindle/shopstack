package dev.irontech.shopstack.reviews;

import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;

@Configuration
public class OutboundConfig {

    /**
     * An outbound channel adapter listens to new messages from a Spring channel and publishes
     * them to a Google Cloud Pub/Sub topic.
     * @param pubsubTemplate
     * @return
     */
    @Bean
    @ServiceActivator(inputChannel = "pubsubOutputChannel")
    public MessageHandler messageSender(PubSubTemplate pubsubTemplate, String topic) {
        return new PubSubMessageHandler(pubsubTemplate, topic);
    }

    @MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
    public interface PubSubOutboundGateway {
        void sendToPubsub(String text);
    }
}
