package dev.irontech.shopstack.reviews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    @Value("${reviews.topic}")
    private String topic;

    @Value("${reviews.subscription}")
    private String subscription;

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public String topic() {
        return topic;
    }

    @Bean
    public String subscription() {
        return subscription;
    }
}
