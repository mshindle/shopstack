package dev.irontech.shopstack.reviews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import java.util.Objects;

public class SimpleMessageHandler implements MessageHandler {
    private static final Logger logger = LoggerFactory.getLogger(SimpleMessageHandler.class);

    public SimpleMessageHandler() {
    }

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        logger.info("Message arrived! Payload: {}", new String((byte[]) message.getPayload()));
        BasicAcknowledgeablePubsubMessage originalMessage =
                message.getHeaders().get(GcpPubSubHeaders.ORIGINAL_MESSAGE, BasicAcknowledgeablePubsubMessage.class);
        Objects.requireNonNull(originalMessage).ack();
    }
}
